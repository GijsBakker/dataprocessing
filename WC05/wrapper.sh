#!/usr/bin/env bash
source venv/bin/activate
snakemake --cores 16
deactivate